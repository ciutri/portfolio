import React  from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import { Main } from './pages'
import { BackToTop } from './components'
import ScrollToTop from './utils/ScrollToTop'

import './App.css'
import {themeData} from "./data/themeData";

function App() {

  console.log("%cUrus Portfolio", `color:${themeData.theme.primary}; font-size:50px`);

  return (
      <div className="app">
        <Router>
          <ScrollToTop/>
          <Switch>
            <Route path="/" exact component={Main} />

            <Redirect to="/" />
          </Switch>
        </Router>
        <BackToTop />
      </div>
  );
}

export default App;